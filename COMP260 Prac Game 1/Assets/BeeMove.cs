﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour
{

    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    // private state
    private float speed;
    private float turnSpeed;
    private Transform target;
    private Transform target2;
    private Vector2 heading;
    public ParticleSystem explosionPrefab;

    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        Destroy(explosion.gameObject, explosion.duration);
    }
    void Start()
    {
        // find the player to set the target
        PlayerMove p = FindObjectOfType<PlayerMove>();
        target = p.transform;
        target2 = p.transform;
        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
        Random.value);
    }

    void Update()
    {
        // get the vector from the bee to the target
        Vector2 direction = target.position - transform.position;
        Vector2 direction2 = target2.position - transform.position;
        if (direction.magnitude < direction2.magnitude) {
            // calculate how much to turn per frame
            float angle = turnSpeed * Time.deltaTime;
            // turn left or right
            if (direction.IsOnLeft(heading))
            {
                // target on left, rotate anticlockwise
                heading = heading.Rotate(angle);
            }
            else
            {
                // target on right, rotate clockwise
                heading = heading.Rotate(-angle);
            }
            transform.Translate(heading * speed * Time.deltaTime);
        }
        else
        {
            // calculate how much to turn per frame
            float angle = turnSpeed * Time.deltaTime;
            // turn left or right
            if (direction2.IsOnLeft(heading))
            {
                // target on left, rotate anticlockwise
                heading = heading.Rotate(angle);
            }
            else
            {
                // target on right, rotate clockwise
                heading = heading.Rotate(-angle);
            }
            transform.Translate(heading * speed * Time.deltaTime);
        }
    }


}